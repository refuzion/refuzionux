RefuzioNUX Project
===================


About
--------

RefuzionUX fuzes todays remix culture and MMOG's (Massive Multiplayer Online Games) with tomorrows NUI (Natural User Interface)-devices, gaming platforms and mobile devices to form an NUX (Natural User eXperience) interactive gaming and multimedia distribution platform that will ignite the next generation of online entertainment.

For more information, please send an email to info@refuzion.com with the specifics of your request.

Web site
--------
Visit http://refuzion.com/ for the latest project updates.

Compiling from source
---------------------

Building from the repository
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
If you're unable to figure it out just by looking at the source then it's likely not something you'll want to attempt building at this point. That said, have at it! Just please hold back from asking questions that start with "How do I..." just yet. We'll be able to answer those questions soon, but for now they will go unanswered.

